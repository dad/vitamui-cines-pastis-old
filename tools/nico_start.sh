#!/usr/bin/env bash

DIR="C:/Users/sbrnc/IdeaProjects/vitam-ui-develop"

function start-si() {
cd $DIR
cd api/api-security/security-internal
echo "------------security internal----------------------"
mvn spring-boot:run -Dspring-boot.run.noverify > log 2>&1 &
cd $DIR
tail -f api/api-security/security-internal/log
}

function start-iami() {
cd $DIR
cd api/api-iam/iam-internal
echo "------------IAM internal----------------------"
mvn spring-boot:run -Dspring-boot.run.noverify > log 2>&1 &
cd $DIR
tail -f api/api-iam/iam-internal/log
}

function start-iame() {
cd $DIR
cd api/api-iam/iam-external
echo "------------IAM external----------------------"
mvn spring-boot:run -Dspring-boot.run.noverify > log 2>&1 &
cd $DIR
tail -f api/api-iam/iam-external/log
}
function start-cas() {
cd $DIR
cd cas/cas-server
echo "------------cas server----------------------"
java -Dspring.config.location=src/main/config/cas-server-application-dev.yml -jar target/cas-server.war  > log 2>&1 &
cd $DIR
tail -f cas/cas-server/log
}

function start-api() {
cd $DIR
cd api/api-security/security-internal
echo "------------security internal----------------------"
mvn spring-boot:run -Dspring-boot.run.noverify > log 2>&1 &
cd $DIR
echo "------------si + iami + iame + cas ----------------------"
cd api/api-iam/iam-internal
echo "------------IAM internal----------------------"
mvn spring-boot:run -Dspring-boot.run.noverify > log 2>&1 &
cd $DIR
cd api/api-iam/iam-external
echo "------------IAM external----------------------"
mvn spring-boot:run -Dspring-boot.run.noverify > log 2>&1 &
cd $DIR
tail -f api/api-security/security-internal/log  api/api-iam/iam-external/log api/api-iam/iam-internal/log
}

function start-uiib() {
cd $DIR
echo "------------UI Identity----------------------"
cd ui/ui-identity
mvn spring-boot:run -Dspring-boot.run.noverify > log 2>&1 &
cd $DIR
tail -f ui/ui-identity/log
}

function start-uipb() {
cd $DIR
echo "------------UI Portal----------------------"
cd ui/ui-portal
mvn spring-boot:run -Dspring-boot.run.noverify > log 2>&1 &
cd $DIR
tail -f ui/ui-portal/log
}

function start-back() {
cd $DIR
echo "------------UI Identity----------------------"
cd ui/ui-identity
mvn spring-boot:run -Dspring-boot.run.noverify > log 2>&1 &
cd $DIR
echo "------------UI Portal----------------------"
cd ui/ui-portal
mvn spring-boot:run -Dspring-boot.run.noverify > log 2>&1 &
cd $DIR
tail -f ui/ui-identity/log  ui/ui-portal/log
}


function start-pif() {
cd $DIR
cd ui/ui-frontend
echo "------------portal install----------------------"
npm install
echo "------------portal Identity----------------------"
npm run start:identity > log-identity-front 2>&1 &
cd $DIR
tail -f ui/ui-frontend/log-identity-front
}

function start-pf() {
cd $DIR
cd ui/ui-frontend
echo "------------portal install----------------------"
npm install
echo "------------portal ----------------------"
npm run start:portal > log-portal-front 2>&1 &
cd $DIR
tail -f ui/ui-frontend/log-portal-front
}

function start-pastis() {
cd $DIR
cd ui/ui-frontend
echo "------------portal install----------------------"
npm install
echo "------------portal ----------------------"
npm run start:pastis > log-pastis 2>&1 &
cd $DIR
tail -f ui/ui-frontend/log-pastis
}

case "$1" in

help)
echo "si:  security internal"
echo "iami: iam internal"
echo "iame: iam external"
echo "api: ******** si + iami + iame"
echo "cas: cas serveur"
echo "uiib: ui identity back"
echo "uipb: ui portal back"
echo "back: ****** uiib + uipb"
echo "pif: ui portal identity front"
echo "pf: portal front"
;;

si)
start-si
;;

iami)
start-iami
;;

iame)
start-iame
;;

cas)
start-cas
;;

api)
start-api
;;

uiib)
start-uiib
;;

uipb)
start-uipb
;;

back)
start-back
;;

pif)
start-pif
;;

pf)
start-pf
;;

pastis)
start-pastis
;;

esac

