import { Component, OnInit, ViewChild, Input, ContentChild, TemplateRef } from '@angular/core';
import { MatTableDataSource, MatSort } from '@angular/material';



@Component({
  selector: 'app-pastis-table',
  templateUrl: './pastis-table.component.html',
  styleUrls: ['./pastis-table.component.scss']
})
export class PastisTableComponent implements OnInit {

  @Input()
  displayedColumns: string[];
  
  @Input()
  data : any;

  dataSource = new MatTableDataSource(this.data);

  @ViewChild(MatSort, {static: true}) sort: MatSort;


  @ContentChild(TemplateRef, { static: true })
  @Input()
  layoutTemplate: TemplateRef<any>;

  constructor() { }

  ngOnInit() {
    this.dataSource.sort = this.sort;
    console.log("DATA : ", this.data);
  }

  checkElement(element:any) {
    let reg= /^(\d+)(,\s*\d+)*/g;
    let isArray=false;
    for (var index in element) {
      if (reg.test(element[index]) || Array.isArray(element[index])) {
        isArray=true;
        return isArray;
      }
    }
    //return true;
      //console.log(element)
     // if (Array.isArray(element)){
       // console.log("Length : ", Array.isArray(element), element[i])
      //}
    //}
  }

}
