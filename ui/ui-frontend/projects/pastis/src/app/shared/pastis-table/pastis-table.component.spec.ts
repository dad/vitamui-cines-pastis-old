import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PastisTableComponent } from './pastis-table.component';

describe('PastisTableComponent', () => {
  let component: PastisTableComponent;
  let fixture: ComponentFixture<PastisTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PastisTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PastisTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
