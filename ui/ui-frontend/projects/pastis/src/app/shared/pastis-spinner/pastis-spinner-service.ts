import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn:'root'
})
export class PastisSpinnerService {
  isLoading = new BehaviorSubject<boolean>(false);

  show() {
      this.isLoading.next(true);
      console.log("Loader is on..")
  }
  hide() {
      this.isLoading.next(false);
      console.log("Loader is off..")
  }

}
