import { Injectable, Injector } from '@angular/core';
import {
  HttpEvent,
  HttpRequest,
  HttpHandler,
  HttpInterceptor,
  HttpErrorResponse,
  HttpResponse
} from '@angular/common/http';
import { Observable, throwError, EMPTY, of } from 'rxjs';
import { finalize, delay, catchError, take, exhaustMap, retry, timeout, tap } from 'rxjs/operators';
import { PastisSpinnerService } from './pastis-spinner-service';
import { ToastrService } from 'ngx-toastr';
import { LoggingService } from 'src/app/core/services/logging.service';

@Injectable({
  providedIn: 'root'
})
export class PastisSpinnerInterceptor implements HttpInterceptor {
  constructor(public loaderService: PastisSpinnerService,
    public loggingService: LoggingService,
    public toasterService: ToastrService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    console.log('Inside loaderInterceptor...');

    const sedaRequest = req.clone();
    const fileTreeRequest = req.clone();
    const downloadFileRequest = req.clone();

    this.loaderService.show();
    return next.handle(req).pipe(
      tap(evt => {
        if (evt instanceof HttpResponse && new RegExp('createprofile').test(req.url)) {
          if (evt.body) {
            this.loggingService.showSuccess('Les données RNG ont été chargés avec succès');
            this.loaderService.hide()
          }
        }
        if (evt instanceof HttpResponse && new RegExp('seda').test(req.url)) {
          if (evt.body) {
            this.loggingService.showSuccess('Les données SEDA ont été chargés avec succès');
            this.loaderService.hide()
          }
        }
        if (evt instanceof HttpResponse && new RegExp('updateprofile').test(req.url)) {
          if (evt.body) {
            this.loggingService.showSuccess('Le fichier RNG a été generé avec succès');
            this.loaderService.hide()
          }
        }
        finalize(() => this.loaderService.hide())
      },
      ),
      catchError((error: any) => {
        if (error instanceof HttpErrorResponse) {
          try {
            this.handleError(error);
          } catch (error) {
            this.handleError(error);
          }
          //log error 
        }
        return of(error);
      }));
  }


  handleError(error: HttpErrorResponse) {
    if (new RegExp('createprofile').test(error.url)) this.loggingService.showError('Échec lors du chargement du fichier RNG');
    if (new RegExp('seda').test(error.url)) this.loggingService.showError('Échec lors du chargement des données SEDA');
    if (new RegExp('updateprofile').test(error.url)) this.loggingService.showError('Échec lors de la géneration du fichier RNG');
    //return throwError('Pastis error ' + error.message);
  }

}
