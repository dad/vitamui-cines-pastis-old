import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PastisSpinnerComponent } from './pastis-spinner.component';

describe('PastisSpinnerComponent', () => {
  let component: PastisSpinnerComponent;
  let fixture: ComponentFixture<PastisSpinnerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PastisSpinnerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PastisSpinnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
