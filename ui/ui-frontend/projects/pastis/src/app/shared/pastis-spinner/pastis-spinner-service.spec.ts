import { TestBed } from '@angular/core/testing';

import { PastisSpinnerService } from './pastis-spinner-service';

describe('LoaderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PastisSpinnerService = TestBed.get(PastisSpinnerService);
    expect(service).toBeTruthy();
  });
});
