import { Component, OnInit, Inject, ViewChild, AfterViewInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSelectionList } from '@angular/material';
import { SedaService } from '../../core/services/seda.service';
import { FileNode, CardinalityConstants } from '../../file-tree/classes/file-node';
import { SedaData } from '../../file-tree/classes/seda-data';

export interface PastisSimpleDialogData {
  titleDialog: string;
  fileNode: FileNode;
  sedaNode: SedaData;
}
@Component({
  selector: 'app-pastis-dialog',
  templateUrl: './pastis-dialog.component.html',
  styleUrls: ['./pastis-dialog.component.scss']
})

export class PastisDialogComponent implements OnInit, AfterViewInit {

  sedaData:SedaData;
  allowedChildren : SedaData[];
  sedaNodeFound: SedaData;
  selectedSedaNode:SedaData ;
  selectedSedaElement:string = '';
  
  loading:boolean;

  dialogData: PastisSimpleDialogData;

  @ViewChild(MatSelectionList, { static: false })
  sedaListElements: MatSelectionList;

  constructor(
    public dialogRef: MatDialogRef<PastisDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public dialogReceivedData: PastisSimpleDialogData,
    public sedaService: SedaService) {
    }

  ngOnInit() {
    this.sedaService.getSedaRules().subscribe(data => {

      this.sedaData = data[0];
      console.log("Seda data on init of pastis dialog : %o", this.sedaData);
      console.log("Data received on dialog : %o", this.dialogReceivedData);
      this.sedaService.getSedaNode(this.sedaData, this.dialogReceivedData.fileNode.name).subscribe(data=>{
        this.sedaNodeFound = data;
        this.allowedChildren = this.sedaService.findAllowedChildren(this.sedaNodeFound,this.dialogReceivedData.fileNode);
      })
    })
  }  

  selectSedaElement(event) {
    this.sedaService.getSedaNode(this.sedaData,event).subscribe(data=>{
      this.selectedSedaNode = data;
      this.sedaService.selectedSedaNode.next(this.selectedSedaNode);
      console.log("Selected Seda Node : %o..seda element %o", this.selectedSedaNode, this.selectedSedaElement);
    });


  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onYesClick(): void {
    //this.dialogReveivedData.nodeName = this.selectedSedaElement[0];
    console.log("Clicked ok on dialog : %o" , this.selectedSedaNode);
  }


  getToolTipData(data) {
    if (data && data.length) {
      return data.nodeName
    }
  }
  ngAfterViewInit(): void {
    this.sedaListElements.focus()
    //console.log(this.sedaNodeFound)
    //this.loaderService.isLoading.subscribe(status => {
    //  this.loading = status;
   // })
  }
  ngOnDestroy(): void {
  //this.sedaService.selectedSedaNode.next(this.sedaNodeFound);

  //console.log("On destroy  : %o", this.sedaService.selectedSedaNode.getValue() )
  //this.sedaService.selectedSedaNode.unsubscribe();
    
  }

}
