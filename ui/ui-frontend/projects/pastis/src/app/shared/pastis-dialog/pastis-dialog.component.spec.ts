import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PastisDialogComponent } from './pastis-dialog.component';

describe('PastisDialogComponent', () => {
  let component: PastisDialogComponent;
  let fixture: ComponentFixture<PastisDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PastisDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PastisDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
