import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material/table';

import { MatDialogModule,
  MatFormFieldModule, 
  MatInputModule,
  MatButtonModule,
  MatListModule, 
  MatTooltipModule} from "@angular/material";

import { ToastrModule } from 'ngx-toastr';


import { PastisSpinnerComponent } from './pastis-spinner/pastis-spinner.component';
import { PastisDialogComponent } from './pastis-dialog/pastis-dialog.component';
import { PastisUnderConstructionComponent } from './pastis-under-construction/pastis-under-construction.component';
import { PastisTableComponent } from './pastis-table/pastis-table.component';


@NgModule({
  declarations: [
    PastisSpinnerComponent,
    PastisDialogComponent,
    PastisUnderConstructionComponent,
    PastisTableComponent,
  ],
  imports: [CommonModule,
    FormsModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatListModule,
    MatTooltipModule,
    MatTableModule,
    ToastrModule.forRoot({
      timeOut: 3000,
      positionClass: 'toast-bottom-right',
      preventDuplicates: true,
    }),
],
  entryComponents: [PastisDialogComponent],
  exports: [
    PastisSpinnerComponent,PastisUnderConstructionComponent,PastisTableComponent
  ],
})
export class SharedModule {}
