import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PastisUnderConstructionComponent } from './pastis-under-construction.component';

describe('PastisUnderConstructionComponent', () => {
  let component: PastisUnderConstructionComponent;
  let fixture: ComponentFixture<PastisUnderConstructionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PastisUnderConstructionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PastisUnderConstructionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
