import { TestBed } from '@angular/core/testing';

import { SedaService } from './seda.service';

describe('SedaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SedaService = TestBed.get(SedaService);
    expect(service).toBeTruthy();
  });
});
