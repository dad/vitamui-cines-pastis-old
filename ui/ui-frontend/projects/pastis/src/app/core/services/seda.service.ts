import { Injectable } from '@angular/core';
import { PastisApiService } from './api.pastis.service';
import { Observable, throwError, of, EMPTY, BehaviorSubject, pipe } from 'rxjs';
import { SedaData } from '../../file-tree/classes/seda-data';
import { FileNode, CardinalityConstants } from '../../file-tree/classes/file-node';


@Injectable({
  providedIn: 'root'
})
export class SedaService {

  private getSedaUrl = './assets/seda.json';

  selectedSedaNode = new BehaviorSubject<SedaData>(null);
  selectedSedaNodeParent = new BehaviorSubject<SedaData>(null);


  constructor(private pastisAPI : PastisApiService) {}

  getSedaRules(): Observable<SedaData>{

      return this.pastisAPI.getLocally(this.getSedaUrl);
  }

  getChildrenByName(sedaData, nodeName, prop = '', byIndex = false, arr = []): Observable<SedaData> {
    for (let [index, node] of sedaData.entries()) {
      if (node.Name !== nodeName) return byIndex ? [...arr, index] : node.Children.filter(child => child.Name !== nodeName)
      if (prop.length && node[prop].length) {
        let found = this.getChildrenByName(node[prop], nodeName, prop, byIndex, [
          ...arr,
          index
        ]);
        if (found) return found;
      }
    }
    return EMPTY;
  }

  // getSedaNode(currentNode:SedaData,nameNode:string)  {
  //   var i, currentChild, result;
  //   if (nameNode === currentNode.Name) {
  //     return currentNode;
  //   } else {

  //     // Use a for loop instead of forEach to avoid nested functions
  //     // Otherwise "return" will not work properly
  //     for (i = 0; i < currentNode.Children.length; i += 1) {
  //       currentChild = currentNode.Children[i];

  //       // Search in the current child
  //       result = this.getSedaNode(currentChild,nameNode);

  //       // Return the result if the node has been found
  //       if (result !== false) {
  //         this.selectedSedaNode.next(result);
  //         //console.log("Seda data %o and node name %s", currentNode, nameNode);

  //         return result;
  //       }
  //     }
  //     console.log("No node found");
  //     // The node has not been found and we have no more options
  //     return;
  //   }
  // }



  getSedaNode(currentNode:SedaData, nameNode:string):Observable<SedaData> {
   //console.log("Node on this.findSedaNode : %o", currentNode)
   let i: number, currentChild: any, result: boolean;
   if (nameNode == currentNode.Name ) {
     return of(currentNode);
   } else {
     // Use a for loop instead of forEach to avoid nested functions
     // Otherwise "return" will not work properly
     if (currentNode.Children) {
     for (i = 0; i < currentNode.Children.length; i += 1) {
       currentChild = currentNode.Children[i];
       // Search in the current child
       let result = this.getSedaNode(currentChild,nameNode);
       // Return the result if the node has been found
       if (result) {
         return result;
       }
     }
    }
     // The node has not been found and we have no more options
     return;
   }
  }
  getSedaNodeLocally(currentNode:SedaData, nameNode:string):SedaData {
    //console.log("Node on this.findSedaNode : %o", currentNode)
    let i: number, currentChild: any, result: boolean;
    if (nameNode == currentNode.Name ) {
      return currentNode;
    } else {
      // Use a for loop instead of forEach to avoid nested functions
      // Otherwise "return" will not work properly
      if (currentNode.Children) {
        for (i = 0; i < currentNode.Children.length; i += 1) {
          currentChild = currentNode.Children[i];
          // Search in the current child
          let result = this.getSedaNodeLocally(currentChild,nameNode);
          // Return the result if the node has been found
          if (result) {
            return result;
          }
        }
      } else {
          // The node has not been found and we have no more options
          console.log("No SEDA nodes could be found for ", nameNode);
          return;
      }
    }
   }

  findAllowedChildren(sedaNode:SedaData, fileNode:FileNode) {
    //console.log(CardinalityConstants.optional.valueOf());
    //console.log(sedaNode.Children.forEach(x=> console.log(x.Cardinality, x.Cardinality === CardinalityConstants.Optional.valueOf(), x.Name)));
    //console.log("Find allowed children on seda : %o", sedaNode)
    if (!sedaNode) return;
    return sedaNode.Children.filter(x => fileNode.children
      .filter(y => y.name === x.Name &&
              (x.Cardinality !== CardinalityConstants.Optional.valueOf() ||
              x.Cardinality !== CardinalityConstants["One Or More"].valueOf())
              ).length > 0 ? false : true)
  }



  findAllowedCardinalityList(card, allowedCardinalityMap,cardinalityValues){
    let cardlist=[];
    //console.error("Card", this.allowedCardinality)
    for (const [cardinality, allowedCard] of allowedCardinalityMap) {
      if (cardinality === card) {
        console.log(cardinalityValues.find(c=>c.value == card).value);
          allowedCard.forEach( a => cardlist.push(cardinalityValues.find(c=>c.value == a).value))
      } continue;
    }
    cardlist.length > 0 ? cardlist : cardlist.push(cardinalityValues.find(c=>c.value === '1').value);
    //this.cardinality = cardlist;
    return cardlist;
  }


  findCardinalityName(clickedNode:FileNode, cardlinalityValues){
    if(!clickedNode.cardinality){
      return "1"
    } else {
        return cardlinalityValues.find(c=>c.value == clickedNode.cardinality).value
    }
  }



}


