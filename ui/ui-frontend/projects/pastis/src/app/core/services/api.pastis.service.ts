import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient, } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PastisApiService {
  constructor(
    private http: HttpClient,
  ) {}

  // Generic GET Method
  get(path: string, options?: {}): Observable<any> {
    console.log('On API service using url : ', `${environment.apiPastisUrl}${path}`);
    return this.http.get(`${environment.apiPastisUrl}${path}`, options);

  }

    // Generic GET Method
    getLocally(path: string): Observable<any> {
      console.log('On API service using url : ', `${path}`);
      return this.http.get<any>(`${path}`);
 
    }

  // Generic PUT Method
  put<T>(path: string, body: object = {}): Observable<T> {
    return this.http.put<T>(
      `${environment.apiPastisUrl}${path}`,
      JSON.stringify(body));
    }

  // Generic POST Method
  post<T>(path: string, body: object = {}, options?: {}): Observable<T> {
    console.log('File', body);
    console.log('On api service post with options : ' , options);
    return this.http.post<T>(`${environment.apiPastisUrl}${path}`, body, options);
  }

  delete(path): Observable<any> {
    return this.http.delete(
      `${environment.apiPastisUrl}${path}`);
  }

}

