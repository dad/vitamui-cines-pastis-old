import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient, } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class OntologyApiService {
  constructor( private http: HttpClient) {}

  // Generic GET Method
  get(path: string, options?: {}): Observable<any> {
    return this.http.get(`${environment.apiOntologyUrl}${path}`, options);

  }

  // Generic PUT Method
  put<T>(path: string, body: object = {}): Observable<T> {
    return this.http.put<T>(
      `${environment.apiOntologyUrl}${path}`,
      JSON.stringify(body));
    }

  // Generic POST Method
  post<T>(path: string, body: object = {}, options?: {}): Observable<T> {
    console.log('File', body);
    return this.http.post<T>(`${environment.apiOntologyUrl}${path}`, body, options);
  }

  // Generic POST Method
  delete(path): Observable<any> {
    return this.http.delete(
      `${environment.apiOntologyUrl}${path}`);
  }

}

