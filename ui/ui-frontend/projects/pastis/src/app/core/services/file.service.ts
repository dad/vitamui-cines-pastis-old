import { Injectable, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Observable, throwError, BehaviorSubject } from 'rxjs';
import { FileNode } from '../../file-tree/classes/file-node';
import { ProfileService } from './profile.service';
import { MatDialog } from '@angular/material';
import { PastisDialogComponent } from '../../shared/pastis-dialog/pastis-dialog.component';

@Injectable({
  providedIn: 'root'
})

export class FileService  {
  
  @Output() 
  public addNode: EventEmitter<any> = new EventEmitter();

  dataChange = new BehaviorSubject<FileNode[]>([]);
  nodeChange = new BehaviorSubject<FileNode>(null);
  allData = new BehaviorSubject<FileNode[]>([]);



  private params = {};
  private formatErrors(error: any) {
    return  throwError('An error : '  + error.error);
  }

  constructor(private profileService: ProfileService,private dialog: MatDialog) { }


  getCurrentFileTree(): Observable<FileNode[]> {
      console.log("On file service : ", this.dataChange.getValue())
      return this.dataChange;
  }

  getFileTreeFromApi(): Observable<FileNode[]>  {
    this.profileService.getProfile().subscribe(data => {
      this.dataChange.next(data);
      });
    return this.dataChange;
  }
  updateFileTree(newData): Observable<FileNode[]> {
    this.dataChange.next(newData);
    //console.log('Data change on file service has changed!' + this.dataChange.getValue());
    return this.dataChange;
  }

  updateNode(newNode): Observable<FileNode[]> {
    this.dataChange.getValue().find(
      t => t.name === newNode.name);
    //console.log('Data change on file service has changed!' + this.dataChange.getValue());
    return this.dataChange;
  }

  sendNode(node:FileNode) {
    this.nodeChange.next(node);

    console.log("Node on file file service : ", this.nodeChange.getValue());
  }
  getSelectedNodeFromPop(fileNodeData, sedaNodeData){
    const dialogRef = this.dialog.open(PastisDialogComponent, {
      width: '500px',
      data: { dialogTitle: "A title to show", fileNode: fileNodeData, sedaNode:sedaNodeData}
    });

    return new Promise((resolve, reject) => {
      dialogRef.afterClosed().subscribe(data => {
        if (data) {
          console.log("Pop up data received is : %o", data)
        }
        console.log('The dialog was closed');
        resolve(data);
      }, reject)
    });
  }
  

  



}
