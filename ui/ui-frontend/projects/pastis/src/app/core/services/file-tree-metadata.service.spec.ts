import { TestBed } from '@angular/core/testing';

import { FileTreeMetadataService } from './file-tree-metadata.service';

describe('FileTreeMetadataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FileTreeMetadataService = TestBed.get(FileTreeMetadataService);
    expect(service).toBeTruthy();
  });
});
