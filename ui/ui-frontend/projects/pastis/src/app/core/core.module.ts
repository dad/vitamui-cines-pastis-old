import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule, MatProgressSpinnerModule} from '@angular/material';

import { PastisApiService } from './services/api.pastis.service';
import { OntologyApiService } from './services/api.ontology.service';

import { FileService } from './services/file.service';
import { ProfileService } from './services/profile.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { PastisSpinnerInterceptor } from '../shared/pastis-spinner/pastis-spinner-interceptor';
import { SharedModule } from '../shared/shared.module';
import { SedaService } from './services/seda.service';


@NgModule({
  imports: [
    CommonModule,
    MatButtonModule,
    MatIconModule,
    MatProgressSpinnerModule,
    SharedModule
  ],
  exports: [],
  providers: [ PastisSpinnerInterceptor,
    FileService, 
    ProfileService, 
    PastisApiService, 
    OntologyApiService,
    SedaService,
     { provide: HTTP_INTERCEPTORS, useClass: PastisSpinnerInterceptor, multi: true }],
})
export class CoreModule { 

}