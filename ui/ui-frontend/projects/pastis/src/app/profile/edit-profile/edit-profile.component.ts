import { Component, OnInit, Input, ViewEncapsulation, OnDestroy, ViewChild } from '@angular/core';
import { LoggingService } from 'src/app/core/services/logging.service';
import { PastisSpinnerService } from 'src/app/shared/pastis-spinner/pastis-spinner-service';
import { FileService } from 'src/app/core/services/file.service';
import { FileNode } from 'src/app/file-tree/classes/file-node';
import { Tile } from 'src/app/app.component';
import { MatTabChangeEvent, MatTreeNestedDataSource } from '@angular/material';
import { NestedTreeControl } from '@angular/cdk/tree';
import { BehaviorSubject } from 'rxjs';
import { FileTreeComponent } from 'src/app/file-tree/file-tree.component';
import { SedaService } from 'src/app/core/services/seda.service';
import { SedaData } from 'src/app/file-tree/classes/seda-data';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss',],
  //encapsulation: ViewEncapsulation.None
})

export class EditProfileComponent implements OnInit, OnDestroy {

  nodeToSend: FileNode;

  sedaParentNode: SedaData;

  profileRulesIsLoaded: boolean;
  
  loadRules: boolean;

  activeTabIndex: number;

  tabRulesMap: Map<string, Map<string, string[]>>;
  nodeParentChildMap: Map<string, string[]>;

  nestedTreeControl: NestedTreeControl<FileNode>;
  nestedDataSource: MatTreeNestedDataSource<FileNode>;

  dataChange = new BehaviorSubject<FileNode[]>([]);

  profileTabChildrenToInclude: string[] = [];
  profileTabChildrenToExclude: string[] = [];
  headerTabChildrenToInclude: string[] = [];
  headerTabChildrenToExclude: string[] = [];
  rulesTabChildrenToInclude: string[] = [];
  rulesTabChildrenToExclude: string[] = [];
  treeTabChildrenToInclude: string[] = [];
  treeTabChildrenToExclude: string[] = [];
  objectTabChildrenToInclude: string[] = [];
  objectTabChildrenToExclude: string[] = [];

  rootNames: string[] = [];
  tabLabels: string[] = [];
  tabShowElementRules : string [][][]= [];

  @ViewChild(FileTreeComponent, {static: false}) fileTreeComponent: FileTreeComponent;

  @Input()
  tile: Tile;

  loadingFileTree: boolean

  constructor(private sedaService:SedaService, private fileService: FileService) {

    this.nestedTreeControl = new NestedTreeControl<FileNode>(this.getChildren);
    this.nestedDataSource = new MatTreeNestedDataSource();

    this.tabLabels.push('Profil','En-tête','Régles','Arborescence','Objects');
    this.rootNames.push('','ArchiveTransfer','ManagementMetadata','ArchiveUnit','DataObjectPackage');
    
    // Children to include or exclude
    this.profileTabChildrenToInclude.push();
    this.profileTabChildrenToExclude.push();
    this.headerTabChildrenToInclude.push();
    this.headerTabChildrenToExclude.push('DataObjectPackage');
    this.rulesTabChildrenToInclude.push();
    this.rulesTabChildrenToExclude.push();
    this.objectTabChildrenToInclude.push('DataObjectGroup','BinaryDataObject','PhysicalDataObject')
    this.objectTabChildrenToExclude.push('DescriptiveMetadata','ManagementMetadata','id')
    this.tabShowElementRules.push([this.profileTabChildrenToInclude,this.profileTabChildrenToExclude],
      [this.rulesTabChildrenToInclude,this.rulesTabChildrenToExclude],
      [this.headerTabChildrenToInclude,this.headerTabChildrenToExclude],
      [this.objectTabChildrenToInclude,this.objectTabChildrenToExclude])
  }
  ngOnInit() {
    //console.error("Tab labels : ", this.tabLabels)
    this.activeTabIndex = 1;
    this.isTabClicked(this.activeTabIndex)
    //console.error("Rules : ",this.tabShowElementRules)

    //this.logging.printLog("From app component...");
    this.fileService.getFileTreeFromApi().subscribe(response => {
      if (response) {
        this.nestedDataSource.data = response;
        this.nestedTreeControl.dataNodes = response;
        //this.data = response;
        this.nodeToSend = response[0];
        this.dataChange.next(response);
        
        console.log("Init file tree node on file tree : %o", this.dataChange.getValue()[0]);
      }
    });
    this.sedaService.getSedaRules().subscribe(data=>{
      this.sedaParentNode = data[0];
    })


  }

  isTabClicked(i: number) {
    //console.error("tab is active :",this.activeTabIndex ? true : false)
    return i === this.activeTabIndex ? true : false
  }

  loadProfile(event: MatTabChangeEvent) {
    //console.error(event.index, event.tab.textLabel)
    this.activeTabIndex = event.index;
    this.loadProfileData(event.index);

  }

  loadProfileData(index:number) {
    console.error("The dataChange before : ", this.dataChange.getValue(),
                    "Nested data before : ", this.nestedDataSource)

    let nodeToFilter = this.rootNames[index]
    let currentData = this.dataChange.getValue();
    let fiteredData = []; 
    fiteredData.push(this.getFileNode(currentData,this.rootNames[index]));
    if (fiteredData[0]) {
      let allData = [];
      allData.push(this.nodeToSend);
      this.fileService.allData.next(allData);
      this.nestedDataSource.data = fiteredData;
      this.nestedTreeControl.dataNodes = fiteredData;
      this.sedaService.selectedSedaNodeParent.next(this.sedaService.getSedaNodeLocally(this.sedaParentNode,nodeToFilter))
      //this.dataChange.next(fiteredData[0]);
      console.error("The data has changed! Before : ", this.dataChange.getValue(), "... data is now : ", this.nestedDataSource.data,
              "All data is ",this.fileService.allData.getValue() )
      this.fileTreeComponent.sendNodeMetadata(fiteredData[0]);
      
    }
  }

  //TabIndexRules[0] : The list of children to include
  //TabIndexRules[1] : The list of children to exclude
  getFileNode(currentNode, nodeToFind: string): FileNode {
    if (currentNode) {
      let node = currentNode[0] ? currentNode[0] : currentNode;
      //console.log("Node on this.getSedaNodeLocally : ", node , typeof node )
      let i: number, currentChild: any, result: boolean;
      let resultNode : FileNode;
      //for (let nodeToFind of nodesToFind) {
      if (nodeToFind === node.name) {
        //resultNodes.push(node);
        return node;
      } else {
        // Use a for loop instead of forEach to avoid nested functions
        // Otherwise "return" will not work properly
        if (node.children) {
          for (i = 0; i < node.children.length; i += 1) {
            currentChild = node.children[i];
            // Search in the current child
            let result = this.getFileNode(currentChild, nodeToFind);
            // Return the result if the node has been found
            if (result) {
              return result;
            }
          }
        } else {
          // The node has not been found and we have no more options
          console.log("No file nodes could be found for ", nodeToFind);
          return;
        }
      }
    //}
    }
  }



  getChildren = (node: FileNode) => node.children;


  ngOnDestroy() {
  }

}
