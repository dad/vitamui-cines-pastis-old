import { Component, OnInit, OnDestroy, ViewChild, AfterViewInit, ChangeDetectionStrategy, SimpleChanges, } from '@angular/core';
import { FileNode } from './file-tree/classes/file-node';
import { FileService } from './core/services/file.service';
import { LoggingService } from './core/services/logging.service'
import { CdkTextareaAutosize } from '@angular/cdk/text-field/typings/autosize';
import { PastisSpinnerService } from './shared/pastis-spinner/pastis-spinner-service';

export interface Tile {
  color: string;
  cols: number;
  rows: number;
  text: string;
  outlinecolor: string;
  icon: string;
  class: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  //changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent implements OnInit {

  @ViewChild('treeSelector', { static: true }) tree: any;

  @ViewChild('autosize', { static: false }) autosize: CdkTextareaAutosize;

  nodeToSend:FileNode;


  loading = false;

  events: string[] = [];
  opened: boolean;

  shouldRun = true;

  tiles: Tile[] = [
    { text: 'Profil d\'archivage', cols: 2, rows: 2, color: 'lightgreen', outlinecolor: 'grey', icon: 'edit',class:'pastis-slide-nav-profile'},
    { text: 'Options', cols: 10, rows: 1, color: 'orange', outlinecolor: 'grey', icon: 'apps', class:'pastis-header-with-sidebav' },
    { text: 'Metadonnes', cols: 10, rows: 1, color: 'lightblue', outlinecolor: 'grey', icon: 'star',class:'pastis-center-content' },
  ];

  constructor(private spinnerService: PastisSpinnerService) {}

  ngOnInit() {


  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.spinnerService.isLoading.subscribe(status => {
        this.loading = status;
        console.log("Status from behavior : ", status)
      });
    });
    console.log("After check : ", this.loading)

  }

}
