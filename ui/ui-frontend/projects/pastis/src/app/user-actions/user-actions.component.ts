import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Tile } from '../app.component';
import { FileNode } from '../file-tree/classes/file-node';

import { ProfileService } from '../core/services/profile.service';
import { FileService } from '../core/services/file.service';

@Component({
  selector: 'app-actions',
  templateUrl: './user-actions.component.html',
  styleUrls: ['./user-actions.component.scss'],
  providers: []
})
export class UserActionsComponent implements OnInit, OnDestroy {

  @Input()
  tile: any = {};

  loading = false;

  data: FileNode[] = [];

  constructor(private profileService: ProfileService, private fileService: FileService) { }

  ngOnInit() {
 //   this.loaderService.isLoading.subscribe(status => {
 //     this.loading = status;
 //   })
  }
  loadProfile() {  }

  postJsonToRNG() {
    //Retrieve the current file tree data as a JSON
    //this.fileService.nodeChange.subscribe( currentData => {
      let myData = this.fileService.allData.getValue();
      this.data = myData;
      console.log("On export button postJsonToRNG with current data %o",this.fileService.allData.getValue());
      if (this.data) {
        // Send the retrieved JSON data to profile service
        this.profileService.updateFile(this.data).subscribe(retrievedData => {
          console.log("Data profile service: " + retrievedData)
          //this.nestedDataSource.data = data;
          // this.data = retrievedData;
          console.log('New updated data: ',  this.data);
          //this.nestedTreeControl.dataNodes = data;
          console.log('Data: ', retrievedData);
          this.downloadFile(retrievedData);
        });
      }
    //  });
  }



  downloadFile(json): any {
    const newBlob = new Blob([json], { type: 'application/xml' });
    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveOrOpenBlob(newBlob);
        return;
    }
    const data = window.URL.createObjectURL(newBlob);
    const link = document.createElement('a');
    link.href = data;
    link.download = 'pastis_profile.rng';
    // this is necessary as link.click() does not work on the latest firefox
    link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));
    setTimeout( () => {
        // For Firefox it is necessary to delay revoking the ObjectURL
        window.URL.revokeObjectURL(data);
        link.remove();
    }, 100);
}

  ngOnDestroy(){

  }

}
