import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FileUploadModule } from 'ng2-file-upload';
import { MatIconModule,MatButtonModule } from '@angular/material'

import { UserActionUploadComponent } from '../user-action-upload/user-action-upload.component';

@NgModule({
  declarations: [UserActionUploadComponent],
  imports: [
    CommonModule,
    MatIconModule,
    MatButtonModule,
    FileUploadModule
  ],
  exports: [UserActionUploadComponent ],
})
export class UserActionsModule {
 
 }
