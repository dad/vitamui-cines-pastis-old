import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserActionUploadComponent } from './user-action-upload.component';

describe('UserActionUploadComponent', () => {
  let component: UserActionUploadComponent;
  let fixture: ComponentFixture<UserActionUploadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserActionUploadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserActionUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
