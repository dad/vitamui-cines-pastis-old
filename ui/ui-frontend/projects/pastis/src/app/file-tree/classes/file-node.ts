
/**
 * Json node data with nested structure. Each node has a name and a value or a list of children
 */
export enum TypeConstants {
  element = 'Element',
  attribute = 'Attribute'
}

/**
 * Json node data with nested structure. Each node has a name and a value or a list of children
 */
/**
 * Json node data with nested structure. Each node has a name and a value or a list of children
 */
export enum CardinalityConstants {
  'Zero or More' = '0-N',
  'One Or More' = '1-N',
  'Optional' = '0-1',
  'Obligatoire' = '1',
}


/**
 * Json node data with nested structure. Each node has a name and a value or a list of children
 */
export enum DataTypeConstants {
    string = 'String',
    dateTime = 'DateTime',
    date = 'Date',
    ID = 'ID',
    anyURI = 'Any URI',
    token = 'Token',
    tokenType = 'Token Type',
    undefined = 'Undefined'
}

/**
 * Json node data with nested structure. Each node has a name and a value or a list of children
 */
export enum ValueOrDataConstants {
    value = 'Value',
    data = 'Data',
    nsName = 'nsName',
    undefined = 'Undefined'
}

export class FileNode {
  id: number;
  parentId: number;
  name: string;
  valueOrData: ValueOrDataConstants;
  value: string;
  type: string;
  dataType: DataTypeConstants;
  cardinality: string;
  level: number;
  documentation: string;
  children: FileNode[];
}