export class SedaData {
    Name:string;
    Type:string;
    Element:string;
    Cardinality:string;
    Definition:string;
    Extensible:boolean;
    Choice:boolean;
    Children: SedaData[];
}