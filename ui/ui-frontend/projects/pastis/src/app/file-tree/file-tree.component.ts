import { Component, OnInit, Input, OnDestroy, ViewChild, Output } from '@angular/core';
import { FileService } from '../core/services/file.service';
import { NestedTreeControl } from '@angular/cdk/tree';
import { MatTreeNestedDataSource, MatDialog } from '@angular/material';
import { FileNode, TypeConstants, ValueOrDataConstants, DataTypeConstants, CardinalityConstants } from './classes/file-node';
import { BehaviorSubject, throwError } from 'rxjs';
import { CdkTextareaAutosize } from '@angular/cdk/text-field';
import { PastisSpinnerService } from '../shared/pastis-spinner/pastis-spinner-service';

import { SedaData } from './classes/seda-data';
import { SedaService } from '../core/services/seda.service';
import { LoggingService } from '../core/services/logging.service';
import { Tile } from '../app.component';
import { FileTreeMetadataService } from './file-tree-metadata/file-tree-metadata.service';

@Component({
  selector: 'app-file-tree',
  templateUrl: './file-tree.component.html',
  styleUrls: ['./file-tree.component.scss']
})
export class FileTreeComponent implements OnInit, OnDestroy {


  @ViewChild('treeSelector', { static: true }) tree: any;
  @ViewChild('autosize', { static: false }) autosize: CdkTextareaAutosize;


  @Input()
  nestedTreeControl: NestedTreeControl<FileNode>;
  @Input()
  nestedDataSource: MatTreeNestedDataSource<FileNode>;
  @Input()
  rootElement:string;
  @Input()
  rootElementShowName:string;
  @Input()
  childrenListToExclude:string[];
  @Input()
  childrenListToInclude:string[];
  @Input()
  shouldLoad:boolean;

  nodeToSend:FileNode;

  //@Input()
  data: FileNode;
  
  @Input()
  tile: Tile;

  loading: boolean;

  newNodeName : string;

  sedaData:SedaData;

  selectedNode:SedaData;

  treeData : FileNode[];
  parentNodeMap = new Map<FileNode, FileNode>();

  dataChange = new BehaviorSubject<FileNode>(null);

  typeType = TypeConstants;
  valueOrDataType = ValueOrDataConstants;
  dataTypeType = DataTypeConstants;
  cardinalityType = CardinalityConstants;

  dividerSizer : string;
  

  constructor(private fileService: FileService, private loggingService : LoggingService, 
    private fileMetadataService : FileTreeMetadataService,
    private sedaService:SedaService) { }

  ngOnInit() {
    if (this.shouldLoad) {
      this.sedaService.getSedaRules().subscribe(data=> {
        this.sedaData = data[0];
        this.selectedNode = data[0];
        this.sedaService.selectedSedaNodeParent.next(this.sedaData);
        console.log("Init seda node on file tree : %o" ,this.selectedNode);
      })
      this.fileService.addNode.subscribe(node=>{
        this.addNewItem(node)
      })
    }

    //console.log("Children to exclude : ", this.childrenListToExclude);

  }



  // Use the file.service to send the updated data to reload the tree
  reloadProfile(updatedData) {
    console.log('Data in updateProfile : ', updatedData);
    this.fileService.updateFileTree(updatedData).subscribe(newData => {
      this.nestedDataSource.data = newData;
      this.data = newData[0];
      this.nestedTreeControl.dataNodes = newData;
      //if (updatedData) { this.loading = false; }
      
    });
  }

  getChildren = (node: FileNode) => node.children;

  hasNestedChild(num: number, nodeData: FileNode) {
    return !nodeData.type;
  }




  parentFun(){
    console.log("In file tree")
  }

  /** Select the category so we can insert the new item. */
  async addNewItem(node: FileNode) {

    const sedaNode = <SedaData>await this.fileService.getSelectedNodeFromPop(node, this.selectedNode);
    this.sedaService.selectedSedaNode.next(sedaNode);
    this.insertItem(node, sedaNode);
    this.nestedTreeControl.expand(node);
    // console.log(this.nestedTreeControl);
    this.renderChanges();
  }

  /** Add an item Tree node */
  insertItem(parent: FileNode, sedaNode: SedaData) {
    let newNode = new FileNode();
    if (parent.children && sedaNode) {
      // Get an index based on last parent child
      let lastChildIndex = parent.children.length == 0 ? 0 : parent.children.length - 1;
      let newId = parent.children.length == 0 ? parent.id + 1 : parent.children[lastChildIndex].id + 1;
      //console.log("New id: " + newId)
      
      newNode.name = sedaNode.Name;
      newNode.id = newId;

      newNode.level = parent.level + 1;
      //console.log("Type : " + Object.keys(C).filter(e => e === 'element'));
      newNode.type = Object.keys(TypeConstants).find(e => e === 'element');
      newNode.parentId = parent.id;
      newNode.children = [];
      newNode.cardinality = Object.values(CardinalityConstants).find(c => c.valueOf() === sedaNode.Cardinality);
      //newNode.id = newNode.level + ((parent.children.length + 1) / 10.0);
      //console.log("New node data : %O", newNode);
      console.log("Parent node name: " + parent.name);
      console.log("New node level  : " + newNode.level);
      console.log("New node parent level  : " + parent.level);
      //console.log("New node lenght : " + parent.children.length);
      console.log("New node id : " + newNode.id);
      console.log("New node : %o", newNode);

      parent.children.push(newNode);
      this.parentNodeMap.set(newNode, parent);
      console.log("Set new node name: " + newNode.name);

      console.log("Seda children and file children: ", this.selectedNode.Children,parent.children)

      let sedaChildrenName : string[] = [];
      this.sedaService.getSedaNodeLocally(this.sedaData,parent.name).Children.forEach(child=>{
        sedaChildrenName.push(child.Name);
      })

      parent.children.sort((a,b)=>{
        return sedaChildrenName.indexOf(a.name) - sedaChildrenName.indexOf(b.name)
      })
      //this.selectedNode = sedaNode;
      //this.sendNodeMetadata(newNode);
      this.sedaService.selectedSedaNode.next(sedaNode);
      this.updateItem(parent);
      this.sendNodeMetadata(parent);
      console.log("New fileNode data is : %o" ,this.dataChange.getValue())
    } else {
      /** No nodes will be added, but the subject value needs to updated to the last clicked value. 
       * Otherwise, some components that depend on the last 'clicked' seda value will not behave properlly*/
      this.sedaService.selectedSedaNode.next(this.selectedNode);
      console.log('No More Nodes can be inserted : No node was selected or node name is invalid');
    }

    //}
  }

  sendNodeMetadata(node: FileNode) {
      //if (data) {
      let sedaParent = this.sedaService.selectedSedaNodeParent.getValue()
      let theNode = this.sedaService.getSedaNodeLocally(sedaParent,node.name);
      console.log("Node clicked : ",node, theNode,"...seda node on sevice : ", sedaParent);

      if (theNode) {
        console.log("The found node on filetree : ", theNode)
        this.sedaService.selectedSedaNode.next(theNode);
        this.fileService.dataChange.next(node[0]);
        this.fileService.sendNode(node);
        
        let dataTable = this.fileMetadataService.fillDataTable(theNode,node);
        console.log("Data revtried on click : ", dataTable);
        console.log("Node seda %s in filetree is ready to be edited with seda data %o",node.name,this.sedaService.selectedSedaNode.getValue());
        this.fileMetadataService.dataSource.next(dataTable);


      }
  }

  renderChanges() {
    const data = this.nestedDataSource.data;
    this.nestedDataSource.data = null;
    this.nestedDataSource.data = data;
  }

  remove(node: FileNode) {
    //console.error('currentNode', node);
    this.removeItem(node,this.fileService.dataChange.getValue()[0]);
    this.renderChanges();
  }

  isSedaNodeObligatory(nodeName:string):boolean{
    if (this.sedaData){
      for (let child of this.sedaData.Children) {
        if (child.Name === nodeName){
          //console.error(child.Name )
          return child.Cardinality !== '1' ? true : false;
        }
      } 
    }
  }

  buildFileTree(obj: object, level: number): FileNode[] {
    // This should recive Root node of Tree of Type FileNode
    // so we dont have to create a new node and use it as it is
    return Object.keys(obj).reduce<FileNode[]>((accumulator, key) => {
      // console.log(key);
      const value = obj[key];
      const node = new FileNode();
      node.id = level;

      node.level = level;
      node.name = key;
      node.parentId = null;
      if (value != null) {
        if (typeof value === 'object') {
          node.children = this.buildFileTree(value, level + 1);
        } else {
          node.type = value;
        }
      }
      return accumulator.concat(node);
    }, []);
  }

  /** Remove an item Tree node */
  removeItem(currentNode: FileNode, root: FileNode) {
    // const parentNode = this.parentNodeMap.get(currentNode);
    const parentNode = this.findParent(currentNode.parentId, root);
    //console.log('parentNode ' + JSON.stringify(parentNode))
    const index = parentNode.children.indexOf(currentNode);
    if (index !== -1) {
      parentNode.children.splice(index, 1);
      this.parentNodeMap.delete(currentNode);
      this.dataChange.next(this.data);
    }
     console.log("Deleted node : ", currentNode, "and his parent : ", parentNode);
     // Update the metadata tree
     this.sendNodeMetadata(parentNode);
  }

  /** Update an item Tree node */
  updateItem(node: FileNode) {
    this.dataChange.next(node);
    console.log("Node updated to : ",this.dataChange.getValue())

  }

  /** Find a parent tree node */
  findParent(id: number, node: any): any {
    
    //console.log("SEDA NODE children ON REMOVE : ", node.children);

    
    if (node && node.id === id) {
      return node;
    } else {
      //console.log('ELSE ' + JSON.stringify(node.children));
      if (node.children && id) {
        for (let element = 0; node.children.length; element ++) {
          //console.log('Recursive ' + JSON.stringify(node.children[element].children));
          //console.error("Node here : ", node.children[element], element)
          if (element && node.children && node.children.length > 0 && node.children[element].children.length > 0) {
            return this.findParent(id, node.children[element]);
          } else {
              continue;
          }
        }
      }
    }
  }

  findParentLevel(nodeToFind:FileNode) {
    let parentNodeToSearch = this.nestedDataSource.data;
    for (let node of parentNodeToSearch) {
      // For nested elements
      if (this.rootElement === node.name && this.rootElement === nodeToFind.name && 
        parentNodeToSearch[0].name === node.name && parentNodeToSearch[0].id !== nodeToFind.id) {
       return 1;
      }
      return nodeToFind.level - node.level;
    }
  }

  childIsIncluded(node:FileNode){
    if (this.childrenListToInclude) {
      let childrenToInclude = this.childrenListToInclude
      return childrenToInclude.includes(node.name); 
    } 
    return true;
  }

  childIsExcluded(node:FileNode){
    if (this.childrenListToExclude) {
      let childrenToExclude = this.childrenListToExclude
      //console.error("On excluded : ", childrenToExclude.includes(node.name));
      return childrenToExclude.includes(node.name);
    }
    return false;
  }
  
  

  /** Error handler */
  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

  showAllowedChidren(node:FileNode){
    if (this.childrenListToExclude) {
      return !this.childrenListToExclude.includes(node.name);
    }
  }



  ngAfterViewInit(): void {

    
  }

  ngOnDestroy(): void {
  }
  

}
