import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { FileNode, CardinalityConstants } from '../classes/file-node';
import { SedaData } from '../classes/seda-data';


@Injectable({
  providedIn: 'root'
})
export class FileTreeMetadataService {

  cardinalityValues: CardinalityValues[] = [];
  allowedCardinality: Map<string, string[]>;
  dataSource = new BehaviorSubject<MedatadaHeaders[]>(null);
  selectedCardinalities = new BehaviorSubject<string[]>([]);
  allowedSedaCardinalities = new BehaviorSubject<string[][]>([]);


  constructor() {
    this.initCardinalityValues();
  }



  initCardinalityValues() {
    for (let key in CardinalityConstants) {
      let cardinality: CardinalityValues = { value: CardinalityConstants[key], viewValue: key };
      this.cardinalityValues.push(cardinality);
    }
    this.allowedCardinality = new Map<string, string[]>();
    this.allowedCardinality.set('1', ['1']);
    this.allowedCardinality.set('', ['1']);
    this.allowedCardinality.set(null, ['1']);
    this.allowedCardinality.set(undefined, ['1']);
    this.allowedCardinality.set("null", ['1']);
    this.allowedCardinality.set('0-1', ['0-1', '1']);
    this.allowedCardinality.set('0-N', ['0-1', '0-N', '1-N', '1']);
    this.allowedCardinality.set('1-N', ['1', '1-N']);
  }

  fillDataTable(sedaChild: SedaData, clickedNode: FileNode) {
    let data = [];
    let allowedCardList: string[][];
    if (clickedNode.children.length > 0) {
      for (let child of clickedNode.children) {
        //allowedCardList.push(this.findAllowedCardinalityList2(sedaChild,child.name));
        data.push({
          "Nom du champ": child.name,
          "Valeur fixe": child.value,
          Cardinalité: this.findSedaAllowedCardinalityList(sedaChild, child),
          Comentaire: child.documentation,
          Type: child.dataType
        })
      }
    } else {
      data.push({
        "Nom du champ": clickedNode.name,
        "Valeur fixe": clickedNode.value,
        Cardinalité: this.findSedaAllowedCardinalityList(sedaChild, clickedNode),
        Comentaire: clickedNode.documentation,
        Type: clickedNode.dataType
      })
    }
    this.allowedSedaCardinalities.next(allowedCardList);
    //this.dataSource.next(data);
    this.selectedCardinalities.next(this.findCardinalities(clickedNode, sedaChild));
    console.log("Data on fillDataTable", data, "with selected cards :", this.selectedCardinalities.getValue());
    return data;
  }

  findSedaAllowedCardinalityList(sedaNode: SedaData, clickedNode: FileNode) {
    let allowedCardinalityListResult: string[] = [];
    let resultTest: string[][] = [];
    //console.error("Seda children :", sedaNode.Children.length, "file node : ", clickedNode.name, "children : ", clickedNode.children.length, "card : ",sedaNode.Cardinality);
    //console.error("Clicked node children : ", clickedNode.name,
    //  clickedNode.children.length, sedaNode.Children.length, sedaNode.Name);

    // If the clicked node has the same name was the seda node, the node is already found
    if (sedaNode.Name === clickedNode.name) {
      allowedCardinalityListResult = this.allowedCardinality.get(sedaNode.Cardinality);
      return allowedCardinalityListResult;
    }
    if (sedaNode.Children.length > 0) {
      // Search the sedaNode children to find the correnpondent cardinality list
      for (let child of sedaNode.Children) {
        if ((child.Name === clickedNode.name) || (sedaNode.Name === clickedNode.name)) {
          // Used in the case we wish to "correct" the node's cardinality, since
          // the seda cardinality wont include the cardinality retrieved by node's rng file.
          // In this case, the condition will return the rng file cardinality list
          // instead of node's cardinality list in accordance with the SEDA specification.
          //if (child.Cardinality !== sedaNode.Cardinality){
          //allowedCardinalityListResult = this.allowedCardinality.get(clickedNode.cardinality);
          //return allowedCardinalityListResult;
          //}
          allowedCardinalityListResult = this.allowedCardinality.get(child.Cardinality);
          resultTest.push(allowedCardinalityListResult)
          this.allowedSedaCardinalities.next(resultTest)
          //console.error("Final CARDINALITY LIST (seda children found) : ", allowedCardinalityListResult,
          // " for ", child.Name);
          return allowedCardinalityListResult;
        } else {
          //console.error("Not found on SEDA");
        }
      }
    } else {
      //console.error("Final CARDINALITY LIST (NO seda children found) : ", allowedCardinalityListResult, " for ", sedaNode.Name);
      for (const [card, cardlist] of this.allowedCardinality) {
        if (card === clickedNode.cardinality) {
          !clickedNode.cardinality ? allowedCardinalityListResult.push("1") : allowedCardinalityListResult = cardlist;
          //result = cardlist;
          resultTest.push(cardlist)
          this.allowedSedaCardinalities.next(resultTest)
          //console.error("Final CARDINALITY LIST : ", allowedCardinalityListResult)
          return allowedCardinalityListResult;
        }
      }
    }
    this.allowedSedaCardinalities.next(resultTest)

    if (allowedCardinalityListResult.length < 1) {
      //console.error("Card not found for : ", clickedNode.name, "..assuming attribute cardinality :", clickedNode.cardinality);
      allowedCardinalityListResult = this.allowedCardinality.get(clickedNode.cardinality);
      //!clickedNode.cardinality ? result.push("1") : result = this.allowedCardinality[clickedNode.cardinality];
      return allowedCardinalityListResult;

    }
  }


  findCardinalities(clickedNode: FileNode, sedaNode: SedaData) {
    let childrenCard = []
    //console.error("On findCardinalities with clickedNode : ", clickedNode.name)

    if (sedaNode.Children.length > 0) {
      for (let sedaChild of sedaNode.Children) {
        for (let fileNodechild of clickedNode.children) {
          if (fileNodechild.name === sedaChild.Name) {
            //console.error("Child on findCardinalities", fileNodechild.name, fileNodechild.cardinality, sedaChild.Cardinality, !fileNodechild.cardinality)
            !fileNodechild.cardinality ? childrenCard.push("1") : childrenCard.push(fileNodechild.cardinality);
            //console.error("Cards list values so far....", childrenCard, "..last element : ", fileNodechild.name);
          } else {

          }
        }
      }
    } else {
      //console.error("No clicked node children found for %s. Card must be : ", clickedNode.name, clickedNode.cardinality)
      !clickedNode.cardinality ? childrenCard.push("1") : childrenCard.push(clickedNode.cardinality)
    }
    if (childrenCard.length < 1) {
      !clickedNode.cardinality ? childrenCard.push("1") : childrenCard.push(clickedNode.cardinality)
    }
    //console.error("Final CARDINALITY VALUE : ", childrenCard, " for : ", clickedNode.name);
    return childrenCard;
  }
}


export interface MedatadaHeaders {
  'Nom du champ': string;
  'Type': string;
  'Valeur fixe': string;
  Cardinalité: string[];
  Comentaire: string;
}

export interface CardinalityValues {
  value: string;
  viewValue: string;
}
