import { Component, OnInit, Input, AfterViewInit, Output, NgZone, ViewChild, ViewEncapsulation, OnDestroy, EventEmitter } from '@angular/core';
import { FileNode, TypeConstants, ValueOrDataConstants, DataTypeConstants, CardinalityConstants } from '../classes/file-node';
import { FileService } from '../../core/services/file.service';
import { PastisSpinnerService } from '../../shared/pastis-spinner/pastis-spinner-service';
import { SedaService } from '../../core/services/seda.service';
import { SedaData } from '../classes/seda-data';
import { CdkTextareaAutosize } from '@angular/cdk/text-field';
import { take, first, mergeMap, map, zip } from 'rxjs/operators';
import { FormControl, Validators, FormBuilder, FormGroup } from '@angular/forms';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { thistle } from 'color-name';
import { FileTreeMetadataService } from './file-tree-metadata.service';
import { Observable, forkJoin, concat } from 'rxjs';



export interface CardinalityValues {
  value: string;
  viewValue: string;
}

export interface MedatadaHeaders {
  'Nom du champ': string;
  'Type': string;
  'Valeur fixe': string;
  Cardinalité: string[];
  Comentaire: string;
}


const metadataInfo: MedatadaHeaders[] = [];

@Component({
  selector: 'app-file-tree-metadata',
  templateUrl: './file-tree-metadata.component.html',
  styleUrls: ['./file-tree-metadata.component.scss'],
  // Encapsulation has to be disabled in order for the
  // component style to apply to the select panel.
  encapsulation: ViewEncapsulation.None,
})
/**
 * Returns the average of two numbers.
 *
 * @remarks
 * This method is part of the {@link core-library#Statistics | Statistics subsystem}.
 *
 * @param x - The first input number
 * @param y - The second input number
 * @returns The arithmetic mean of `x` and `y`
 *
 * @beta
 */

export class FileTreeMetadataComponent implements OnInit, AfterViewInit, OnDestroy {

  valueOrData = Object.values(ValueOrDataConstants);
  dataType = Object.values(DataTypeConstants);
  cardinalityList: string[];
  cardinalityLabels = Object.values(CardinalityConstants)

  //Mat table
  matDataSource: MatTableDataSource<MedatadaHeaders>;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  displayedColumns: string[] = ['Nom du champ', 'Valeur fixe', 'Cardinalité', 'Comentaire'];


  @ViewChild('autosize', { static: false }) autosize: CdkTextareaAutosize;

  @Output()
  nodeToAdd  = new EventEmitter<FileNode>();

  @Input()
  tile: any = {};

  loading: boolean;

  clickedNode: FileNode = <FileNode>{};

  selectedNode: SedaData;


  dataTree: FileNode[];

  sedaData: any = {};

  selectedCardinality: string;

  selectedCardinalities: string[];

  allowedSedaCardinalityList: string[][];


  childrenCardinalityMap: Map<string, string[]>;


  allowedCardinality: Map<string, string[]>;

  cardinalityValues: CardinalityValues[] = [];

  valuePattern: string;

  patternType: string;

  panelColor = new FormControl('1');
  
  metadatadaValueFormControl = new FormControl(1, Validators.pattern(this.valuePattern));

  public responseData1: any;
  public responseData2: any;
  public responseData3: any;

  fileNode$: Observable<FileNode>; sedaNode$: Observable<SedaData>;


  constructor(private fileService: FileService, private fileMetadataService: FileTreeMetadataService,
    private sedaService: SedaService, private _ngZone: NgZone, private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.loading = true;
    //Subscription to fileNode service
    this.fileService.getCurrentFileTree().subscribe(fileTree => {
      if (fileTree) {
          this.clickedNode = fileTree[0];
          this.fileService.allData.next(fileTree)
          // Subscription to sedaRules
          this.sedaService.getSedaRules().subscribe(data => {
            if (this.clickedNode) {
            this.sedaService.selectedSedaNode.next(data[0]);
            this.selectedNode = data[0];
            this.getMetadataValuePattern(this.selectedNode.Type)
            this.fileService.nodeChange.next(this.clickedNode)
            let dataTable = this.fileMetadataService.fillDataTable(this.selectedNode, this.clickedNode);
            this.matDataSource = new MatTableDataSource<MedatadaHeaders>(dataTable);
            this.matDataSource.sort = this.sort;
          }
          })
      }
    })

    this.fileMetadataService.selectedCardinalities.subscribe(cards => {
      this.selectedCardinalities = cards;
    });



    // File node
    this.fileService.nodeChange.subscribe(node => {
      this.clickedNode = node;
    })
    
    // Get Current sedaNode
    this.sedaService.selectedSedaNode.subscribe(sedaNode => {
      this.selectedNode = sedaNode;
    })

    // Subscribe to changes in dataSource BehaviorSubject and create a MatTableDataSource with 
    // the new updated data 
    // TODO - matDataSource is being called twice

    
    this.fileMetadataService.dataSource.subscribe(data => {
      this.matDataSource = new MatTableDataSource<MedatadaHeaders>(data);
      //console.error("MatDatasource from behavior value : ", this.matDataSource)
    })
    this.loading = false;
  }
  

  getMetadataValuePattern(type) {
    switch (type) {
      case 'dateTime':
        this.valuePattern = '([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}';
        this.patternType = 'date'
        //console.log("It a date with regex: ", this.valuePattern);
        break;
      case 'TextType':
        this.valuePattern = '^[a-zA-X0-9 ]*$';
        this.patternType = 'text'
        //console.log("It is string with regex: ", this.valuePattern);
        break;
      case 'boolean':
        //console.log("It is a Tuesday.");
        break;
      case 'integer':
        //console.log("It is an  ID");
        break;
    }
  }
  findCardinality(event) {

    if (!event) {
      return CardinalityConstants.Obligatoire;
    } else {
      return event;
    }

  }

  isSedaCardinalityConform(cardList: string[],card:string){
    return cardList.includes(card) ? true : false
  }

  findChildCardinality(name) {
    for (let i = 0; i < this.clickedNode.children.length; i++) {
      if (this.clickedNode.children[i].name === name) {
        //console.error("On fincChildCardinality : ", this.clickedNode.children[i].cardinality);
        return this.clickedNode.children[i].cardinality
      }
      return '1';

    }
  }

  findAllowedCardinalityList(childName = "") {
    let sedaChild: SedaData;
    this.sedaService.getSedaNode(this.selectedNode, childName).subscribe(data => {
      sedaChild = data;
      //console.error("Seda Child: ", sedaChild)
      for (const [cardinality, allowedCard] of this.allowedCardinality) {
        if (cardinality === sedaChild.Cardinality) {
          this.cardinalityList = allowedCard;
          //console.log("The card list of : %s",sedaChild.Name, this.cardinalityList)
          return allowedCard;
        }
      }
    })
  }

  findCardinalityName(clickedNode: FileNode) {
    if (!clickedNode.cardinality) {
      return "1"
    } else {
      return this.cardinalityValues.find(c => c.value == clickedNode.cardinality).value
    }
  }

  setNodeChildrenCardinalities(name, value) {

    if (this.clickedNode.children) {
      for (let i = 0; i < this.clickedNode.children.length; i++) {
        console.log("Event : ", name, value, this.clickedNode.children[i].name, this.clickedNode.children[i].name === name)
        if (this.clickedNode.children[i].name === name) {
          this.clickedNode.children[i].cardinality = value;
          console.log("Changed %s to : ", this.clickedNode.children[i].name,
            this.clickedNode.children[i].cardinality)
        }
      }
    }
    console.error("Cardinalitylist : ", this.cardinalityList)
    console.log(this.clickedNode.children)
  }

  setDocumentation(nodeName:string,evt:string) {
    for (let node of this.clickedNode.children) {
      if (node.name === nodeName) {
        node.documentation = evt;
      }
    }
  }

  //refresh() {
  // // this.fileMetadataService.retrieveNodeMetadata(this.clickedNode).subscribe((data: MedatadaHeaders[]) => {
  //    this.matDataSource.data = data;
  //  });
  //}

  ngAfterViewInit(): void {
  }

  onChange(): void {
  }

  ngOnChanges(changes): void {
    //During initialization, clickedNode is empty, since no button was clicked yet

    if (this.clickedNode && this.selectedNode) {
      this.sedaService.getSedaNode(this.sedaData, this.clickedNode.name).subscribe(data => {
        this.selectedNode = data;
        this.selectedCardinality = this.findCardinalityName(this.clickedNode);
        this.getMetadataValuePattern(this.selectedNode.Type)
        console.error("Selected node on ngOnChanges : %o", this.selectedNode);
        console.error("Cardinality list : ", this.cardinalityList);
      })
    }
  }
  triggerResize() {
    // Wait for changes to be applied, then trigger textarea resize.
    this._ngZone.onStable.pipe(take(1))
      .subscribe(() => this.autosize.resizeToFitContent(true));
  }

  onAddNode(node){
    console.log("The selected node on click is : ", this.fileService.nodeChange.getValue(), this.loading);
    console.log("The parent is : ", this.selectedNode,this.clickedNode)
    this.fileService.addNode.emit(this.clickedNode)

  }

  checkElementType(elementName:string){
    //console.log("Button check", this.sedaService.selectedSedaNode.getValue())
    if (this.selectedNode && !elementName) {
      return this.selectedNode.Element === 'Complex' ? true : false;
    }
    if (this.selectedNode && elementName) {
      for (let node of this.selectedNode.Children){
        if (node.Name === elementName && node.Element === 'Complex') {
          //console.log("Element %s is a complex element", elementName)
          return true;
        }
      }
      //console.log("Element %s is a simple element", elementName)
      return false;
    }
  }

  ngOnDestroy(){
  }
}
