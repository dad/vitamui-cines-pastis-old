import{IEnvironment} from './IEnvironment'

export const environment: IEnvironment= {
  production: false,
  apiPastisUrl: 'http://10.100.129.51:8585/rest',
  apiOntologyUrl: 'http://10.100.129.51:8585',
  name: 'dev'
};
