export interface IEnvironment {
    production: boolean;
    apiPastisUrl: string;
    apiOntologyUrl: string;
    name:string;
}